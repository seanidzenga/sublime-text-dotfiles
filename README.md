# Sublime Text dotfiles

A collection of configurations I use with Sublime Text 3

# External Requirements

- Ruby (required for Asciidoctor) https://www.ruby-lang.org/en/
- Asciidoctor https://asciidoctor.org/docs/install-toolchain/
- Pandoc https://pandoc.org/

# Build systems

- Asciidoc To Docx `Packages/User/Asciidoc To Docx.sublime-build`
- Asciidoc To HTML `Packages/User/Asciidoc To HTML.sublime-build`
- Markdown To Docx `Packages/User/Markdown To Docx.sublime-build`